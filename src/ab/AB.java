
package ab;

public class AB {

    public static void main(String[] args) {
      int dato;
      String nombre;
      
      arbol ab=new arbol();
      
      ab.insertarnodo(22, "Pao");
      ab.insertarnodo(17,"Gaby");
      ab.insertarnodo(23,"Pipe");
      ab.insertarnodo(42,"Matty");
      ab.insertarnodo(51,"Felipe");
      ab.insertarnodo(21,"Rulo");
      ab.insertarnodo(25,"Wuicho");
      ab.insertarnodo(26,"Lupe");
    
    System.out.println("=====InOrden=======");
    if(!ab.vacio()){
        ab.inorden(ab.raiz);
    }
    System.out.println("=====PreOrden=======");
    if(!ab.vacio()){
        ab.preorden(ab.raiz);
    }
    System.out.println("======PosOrden======");
    if(!ab.vacio()){
        ab.posorden(ab.raiz);
    }
    System.out.println("======BUscar======");
    int vb;
    vb=40;
    if(ab.buscarnodo(vb)==null){
        System.out.println("No existe el nodo");
    }
    else{
        System.out.println("Nodo localizado");
        System.out.println();
        System.out.println(ab.buscarnodo(vb).toString());
    }
    System.out.println("======Elimina======");
    int ve;
    ve=10;
    ab.eliminarnodo(ve);
    ab.preorden(ab.raiz);
    
        }
    }
